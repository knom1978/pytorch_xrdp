# An introduction for using docker images as xrdp env
This **Image/Dockerfile** aims to create a container for xrdp.

Refer to https://github.com/danielguerra69/ubuntu-xrdp-docker
## How to use?
* You should git clone this repository first.
```
docker build --no-cache -f Dockerfile_torch1.5.1_py37_cu102 -t xrdp_env/pytorch:1.5.1_py37_cu102 .
docker build --no-cache -f Dockerfile_torch1.4.0_py36_cu101 -t xrdp_env/pytorch:1.4.0_py36_cu101 .
docker build --no-cache -f Dockerfile_torch1.4.0_py36_cu100 -t xrdp_env/pytorch:1.4.0_py36_cu100 .
docker build --no-cache -f Dockerfile_tf2.2_py37_cu101 -t xrdp_env/tf:2.2.0_py37_cu101 .
docker build --no-cache -f Dockerfile_tf1.7.1_py36_cu90 -t xrdp_env/tf:1.7.1_py36_cu90 .
```
* For your pytorch apps, you can use as the hereunder part.
At first, you should run openssl to encode the password to passwordhash.
```
openssl passwd -1 'newpassword'
```
For example, we should encode the password "nthu" to "$1$XKzsPO6z$Z9Gx6K1Y36bjFuqEqqG7T."

Then you can docker run the built image by specific user/password/hostname.

Since the environment variables contain $, you should escape them by "\\" in the command line and "$" in the docker-compose file.
```
docker run -d --name uxrdp --hostname xrdpserver01 --shm-size 1g --runtime=nvidia -e "USER=user01" -e "PASSWORDHASH=\$1\$XKzsPO6z\$Z9Gx6K1Y36bjFuqEqqG7T." -e "IDLETIME=11" -p 33891:3389 -p 2222:22 xrdp_env/pytorch:1.5.1_py37_cu102
docker run -d --name uxrdp --hostname xrdpserver01 --shm-size 1g --runtime=nvidia -e "USER=user01" -e "PASSWORDHASH=\$1\$XKzsPO6z\$Z9Gx6K1Y36bjFuqEqqG7T." -e "IDLETIME=11" -p 33891:3389 -p 2222:22 xrdp_env/pytorch:1.4.0_py36_cu101
docker run -d --name uxrdp --hostname xrdpserver01 --shm-size 1g --runtime=nvidia -e "USER=user01" -e "PASSWORDHASH=\$1\$XKzsPO6z\$Z9Gx6K1Y36bjFuqEqqG7T." -e "IDLETIME=11" -p 33891:3389 -p 2222:22 xrdp_env/pytorch:1.4.0_py36_cu100
docker run -d --name uxrdp --hostname xrdpserver01 --shm-size 1g --runtime=nvidia -e "USER=user01" -e "PASSWORDHASH=\$1\$XKzsPO6z\$Z9Gx6K1Y36bjFuqEqqG7T." -e "IDLETIME=11" -p 33891:3389 -p 2222:22 xrdp_env/tf:2.2.0_py37_cu101
docker run -d --name uxrdp --hostname xrdpserver01 --shm-size 1g --runtime=nvidia -e "USER=user01" -e "PASSWORDHASH=\$1\$XKzsPO6z\$Z9Gx6K1Y36bjFuqEqqG7T." -e "IDLETIME=11" -p 33891:3389 -p 2222:22 xrdp_env/tf:1.7.1_py36_cu90

```

Viola! Then you can connect the starting container through windows remote desktop or xdrp.
